﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<RolesEmployee> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

    }
    public class RolesEmployee
    { 
     public Guid id { get; set; }
    }
}
