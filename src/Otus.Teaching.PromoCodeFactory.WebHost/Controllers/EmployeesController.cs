﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid?>> CreateEmployeeAsync(EmployeeRequest data)
        {
            var requestRoles = new List<RolesEmployee>();
            if (data.Roles != null) {
                requestRoles = data.Roles;
            }
            var employeeData = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Roles = await GetListRoles(requestRoles),
                AppliedPromocodesCount = data.AppliedPromocodesCount
            };
            var result = await _employeeRepository.AddAsync(employeeData);
            return result;
        }
        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<bool>> UpdateEmployeeAsync(Guid id, EmployeeRequest data)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }
            var requestRoles = new List<RolesEmployee>();
            if (data.Roles != null)
            {
                requestRoles = data.Roles;
            }
            var employeeData = new Employee
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Roles = await GetListRoles(requestRoles),
                AppliedPromocodesCount = data.AppliedPromocodesCount
            };
            var result = await _employeeRepository.UpdateAsync(id, employeeData);
            return result;
        }
        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> DeleteAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }
            var result = await _employeeRepository.DeleteByIdAsync(id);

            return result;
        }
        //Получаем список заказанных ролей для пользователя
        private async Task<List<Role>> GetListRoles(List<RolesEmployee> data)
        {
            var roles = new List<Role>();
            if(data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    var role = await _roleRepository.GetByIdAsync(data[i].id);
                    if (role == null)
                    {
                        continue;
                    }
                    roles.Add(role);
                }
            }
            return roles;
        }
    }
}